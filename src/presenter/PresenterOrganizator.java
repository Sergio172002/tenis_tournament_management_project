package presenter;

import model.Referee;
import model.Tenismen;
import model.dao.OrganizatorDAO;
import model.dao.RefereeDAO;
import model.dao.TenismenDAO;
import view.administrator.ViewOrganizator;

import javax.swing.*;
import java.util.List;

public class PresenterOrganizator {

    public PresenterOrganizator(){}

    public static void loginOrganizator(String name, String password){
        OrganizatorDAO cnn = new OrganizatorDAO();
        boolean userFlag = false;
        if(!(cnn.findByNameAndPass(name, password).isEmpty()))
            userFlag = true;

        if(!userFlag) {
            JOptionPane.showMessageDialog(null, "Invalid credentials. Please try again.", "Error", JOptionPane.ERROR_MESSAGE);
        }else{
            new ViewOrganizator().setVisible(true);
        }
    }

    public static List<Tenismen> getListOfPlayers(){
        TenismenDAO tenismen = new TenismenDAO();
        return tenismen.findAll();
    }

    public static List<Referee> getListOfReferees(){
        RefereeDAO referee = new RefereeDAO();
        return referee.findAll();
    }
}
