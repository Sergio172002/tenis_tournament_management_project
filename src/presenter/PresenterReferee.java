package presenter;

import model.Referee;
import model.Tenismen;
import model.dao.RefereeDAO;
import model.dao.TenismenDAO;
import view.referee.viewReferee;

import javax.swing.*;
import java.util.List;

public class PresenterReferee {

    public PresenterReferee(){}

    public static void loginReferee(String name, String password){
        RefereeDAO cnn = new RefereeDAO();
        boolean userFlag = false;
         if(!(cnn.findByNameAndPass(name, password).isEmpty()))
        userFlag = true;

        if(!userFlag) {
            JOptionPane.showMessageDialog(null, "Invalid credentials. Please try again.", "Error", JOptionPane.ERROR_MESSAGE);
        }else{
            new viewReferee().setVisible(true);
        }
    }

    public static List<Tenismen> getListOfPlayers(){
        TenismenDAO tenismen = new TenismenDAO();
        return tenismen.findAll();
    }

    public static List<Referee> getListOfReferees(){
        RefereeDAO referee = new RefereeDAO();
        return referee.findAll();
    }
}
