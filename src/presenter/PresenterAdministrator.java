package presenter;

import model.Referee;
import model.Tenismen;
import model.dao.AdministratorDAO;
import model.dao.RefereeDAO;
import model.dao.TenismenDAO;
import view.administrator.ViewAdministrator;

import javax.swing.*;
import java.util.List;

public class PresenterAdministrator {

    public PresenterAdministrator(){}

    public static void loginAdministrator(String name, String password){
        AdministratorDAO cnn = new AdministratorDAO();
        boolean userFlag = false;
         if(!(cnn.findByNameAndPass(name, password).isEmpty()))
        userFlag = true;

        if(!userFlag) {
            JOptionPane.showMessageDialog(null, "Invalid credentials. Please try again.", "Error", JOptionPane.ERROR_MESSAGE);
        }else{
            new ViewAdministrator().setVisible(true);
        }
    }

    public static List<Tenismen> getListOfPlayers(){
        TenismenDAO tenismen = new TenismenDAO();
        return tenismen.findAll();
    }

    public static List<Referee> getListOfReferees(){
        RefereeDAO referee = new RefereeDAO();
        return referee.findAll();
    }

}
