package view.referee;

import model.dao.ConnectionClass;
import model.dao.RefereeDAO;
import net.proteanit.sql.DbUtils;
import view.tenismen.ViewMethodsTenismen;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static java.lang.Integer.parseInt;

/**
 * Frame-ul corespunzator operatiilor efectuate
 * asupra tabelului Product al bazei de date
 */

public class viewReferee extends JFrame implements ViewMethodsTenismen {

    private JPanel contentPane;
    JTable table = new JTable();
    JButton viewAllGamesBttn = new JButton("View My Games");
    JScrollPane scroll_table = new JScrollPane(table);

    public viewReferee() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(450, 180, 600, 700);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(null);
        setContentPane(contentPane);

        table.setBounds(40,130,90,30);

        scroll_table.setBounds(85, 250, 380, 300);
        scroll_table.setVisible(true);
        contentPane.add(scroll_table);


        viewAllGamesBttn.setFont(new Font("Tahoma", Font.PLAIN, 10));
        viewAllGamesBttn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                viewAllGames();
            }
        });
        viewAllGamesBttn.setBounds(210, 70, 150, 50);
        contentPane.add(viewAllGamesBttn);

    }

    @Override
    public void insertPlayer() {
    }

    @Override
    public void viewAllGames() {
        ConnectionClass cnn = RefereeDAO.printPartTable("arbitru1");
        table.setModel(DbUtils.resultSetToTableModel(cnn.getResult()));
        connection.ConnectionFactory.close(cnn.getStatement());
        connection.ConnectionFactory.close(cnn.getStatement());
        scroll_table.setVisible(true);
    }
}
