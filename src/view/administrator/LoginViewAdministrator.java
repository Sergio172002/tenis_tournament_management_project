package view.administrator;

import presenter.PresenterAdministrator;
import view.ViewMethodsLogin;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;


public class LoginViewAdministrator extends JFrame implements ViewMethodsLogin {

    private JPanel contentPane;
    JLabel timeLimit = new JLabel("Sign In");
    JLabel username = new JLabel("Username: ");
    JTextField tuser = new JTextField();
    JLabel password = new JLabel("Password: ");
    JTextField tpassword = new JTextField();
    JButton registerButton = new JButton("Sign In");

    public LoginViewAdministrator() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(450, 180, 600, 350);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(null);
        setContentPane(contentPane);

        timeLimit.setBounds(260,10,200,40);
        contentPane.add(timeLimit);

        username.setBounds(150, 70, 200, 40);
        contentPane.add(username);

        tuser.setBounds(225, 80, 180, 25);
        contentPane.add(tuser);

        password.setBounds(150, 110, 200, 40);
        contentPane.add(password);

        tpassword.setBounds(225, 120, 180, 25);
        contentPane.add(tpassword);

        registerButton.setFont(new Font("Tahoma", Font.PLAIN, 10));
        registerButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                register();
            }
        });
        registerButton.setBounds(210, 200, 150, 50);
        contentPane.add(registerButton);

        contentPane.setVisible(true);
    }

    @Override
    public void register() {
        setVisible(true);

        String password = tpassword.getText();
        String username = tuser.getText();

        PresenterAdministrator.loginAdministrator(username, password);

    }
}

