package view.administrator;

import model.Organizator;
import model.Referee;
import model.dao.*;
import net.proteanit.sql.DbUtils;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import java.util.ArrayList;

import static java.lang.Integer.parseInt;

/**
 * Frame-ul corespunzator operatiilor efectuate
 * asupra tabelului Product al bazei de date
 */

public class ViewAdministrator extends JFrame implements ViewMethodsAdministrator {

    private JPanel contentPane;
    JLabel timeLimit = new JLabel("Options: ");
    JLabel tenismenName = new JLabel("Name: ");
    JLabel gameId = new JLabel("Id: ");
    JLabel refereeName = new JLabel("Password: ");
    JTable table = new JTable();
    JTable table1 = new JTable();
    JComboBox<String> box3 = new JComboBox<String>();
    JComboBox<String> box1 = new JComboBox<String>();
    JTextField nameTf = new JTextField();
    JTextField passwordTf = new JTextField();
    JTextField gameIdTf = new JTextField("-1");
    JButton addNewGame = new JButton("Add new referee");
    JButton deleteAtIdBttn = new JButton("Delete");
    JButton updateComboBttn = new JButton("Add new organizataor");
    JButton updateTablesBttn = new JButton("Update");
    JButton viewAllGamesBttn = new JButton("View All Users");
    JScrollPane scroll_table = new JScrollPane(table);
    JScrollPane scroll_table1 = new JScrollPane(table1);
    JButton infoBttn = new JButton("I");

    public ViewAdministrator() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(450, 180, 600, 700);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(null);
        setContentPane(contentPane);

        timeLimit.setBounds(180,10,200,40);
        contentPane.add(timeLimit);

        tenismenName.setBounds(40,130,90,30);
        contentPane.add(tenismenName);

        refereeName.setBounds(140,130,90,30);
        contentPane.add(refereeName);

        gameId.setBounds(240,130,90,30);
        contentPane.add(gameId);

        table.setBounds(40,130,90,30);

        scroll_table.setBounds(85, 250, 180, 250);
        scroll_table.setVisible(true);
        contentPane.add(scroll_table);

        scroll_table1.setBounds(360, 250, 180, 250);
        scroll_table1.setVisible(true);
        contentPane.add(scroll_table1);

        box3.setBounds(205, 552, 110, 30);
        contentPane.add(box3);

        box1.setBounds(345, 552, 110, 30);
        contentPane.add(box1);

        gameIdTf.setBounds(240,160,90,30);
        contentPane.add(gameIdTf);

        nameTf.setBounds(40,160,90,30);
        contentPane.add(nameTf);

        passwordTf.setBounds(140,160,90,30);
        contentPane.add(passwordTf);


        addNewGame.setFont(new Font("Tahoma", Font.PLAIN, 10));
        addNewGame.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                addNewReferee();
            }
        });
        addNewGame.setBounds(40, 70, 150, 50);
        contentPane.add(addNewGame);

        updateComboBttn.setFont(new Font("Tahoma", Font.PLAIN, 10));

        updateComboBttn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                addNewOrganizator();
            }
        });
        updateComboBttn.setBounds(220, 70, 150, 50);
        contentPane.add(updateComboBttn);

        viewAllGamesBttn.setFont(new Font("Tahoma", Font.PLAIN, 10));
        viewAllGamesBttn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                   viewAllGames();
            }
        });
        viewAllGamesBttn.setBounds(400, 70, 150, 50);
        contentPane.add(viewAllGamesBttn);

        infoBttn.setBorder(new RoundedBorder(7));
        infoBttn.setFont(new Font("Tahoma", Font.PLAIN, 8));

        infoBttn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setVisible(true);
                updateComboBoxes();
                JOptionPane.showMessageDialog(infoBttn,"You have to set the id of the game you want to modify, "+"\n"+"to choose the field to be modified and to introduce the new value");
            }
        });
        infoBttn.setBounds(490, 550, 35, 35);
        contentPane.add(infoBttn);

        updateTablesBttn.setFont(new Font("Tahoma", Font.PLAIN, 10));
        updateTablesBttn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                updateValues();
            }
        });
        updateTablesBttn.setBounds(40, 545, 150, 50);
        contentPane.add(updateTablesBttn);

        deleteAtIdBttn.setFont(new Font("Tahoma", Font.PLAIN, 10));
        deleteAtIdBttn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

            }
        });
        deleteAtIdBttn.setBounds(40, 605, 150, 50);
        contentPane.add(deleteAtIdBttn);
    }

    @Override
    public void viewAllGames() {
        setVisible(true);

        ConnectionClass cnn = RefereeDAO.printAllTable();
        table.setModel(DbUtils.resultSetToTableModel(cnn.getResult()));
        connection.ConnectionFactory.close(cnn.getStatement());
        scroll_table.setVisible(true);

        ConnectionClass cnn1 = OrganizatorDAO.printAllTable();
        table1.setModel(DbUtils.resultSetToTableModel(cnn1.getResult()));
        connection.ConnectionFactory.close(cnn1.getStatement());
        scroll_table1.setVisible(true);

    }

    @Override
    public void addNewOrganizator() {
        setVisible(true);
        Integer id = Integer.parseInt(gameIdTf.getText());
        String name = nameTf.getText();
        String password = passwordTf.getText();
        Organizator org = new Organizator(id,name,password);
        OrganizatorDAO.insert(org);
    }

    @Override
    public void addNewReferee() {
        setVisible(true);
        Integer id = Integer.parseInt(gameIdTf.getText());
        String name = nameTf.getText();
        String password = passwordTf.getText();
        Referee referee = new Referee(id,name,password);
        RefereeDAO.insert(referee);
    }

    @Override
    public void updateValues() {
        setVisible(true);

        if(box3.getSelectedItem() == "referee") {
            RefereeDAO g = new RefereeDAO();
            Integer id = Integer.parseInt(gameIdTf.getText());
            String value = null;
            if (box1.getSelectedItem() == "name")
                value = (String) nameTf.getText();
            if (box1.getSelectedItem() == "password")
                value = (String) passwordTf.getText();
            g.upadateAtId(id, value, (String) box1.getSelectedItem());
        }
        if(box3.getSelectedItem() == "organizator"){
            OrganizatorDAO g = new OrganizatorDAO();
            Integer id = Integer.parseInt(gameIdTf.getText());
            String value = null;
            if (box1.getSelectedItem() == "name")
                value = (String) nameTf.getText();
            if (box1.getSelectedItem() == "password")
                value = (String) passwordTf.getText();
            g.upadateAtId(id, value, (String) box1.getSelectedItem());
        }
    }

    @Override
    public void updateComboBoxes() {
        List<String> columns = new ArrayList<>();
        columns.add("referee");
        columns.add("organizator");
        for(String a: columns)
            box3.addItem(a);

        List<String> columns1 = new ArrayList<>();
        columns1.add("name");
        columns1.add("password");
        for(String a: columns1)
            box1.addItem(a);
    }
}
