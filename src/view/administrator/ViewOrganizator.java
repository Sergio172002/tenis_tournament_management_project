package view.administrator;

import model.Game;
import model.Referee;
import model.Tenismen;
import model.dao.ConnectionClass;
import model.dao.GameDAO;
import view.ViewMethodsOrganizator;
import net.proteanit.sql.DbUtils;
import presenter.PresenterOrganizator;
import view.administrator.RoundedBorder;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Objects;

import java.util.ArrayList;

import static java.lang.Integer.parseInt;

/**
 * Frame-ul corespunzator operatiilor efectuate
 * asupra tabelului Product al bazei de date
 */

public class ViewOrganizator extends JFrame implements ViewMethodsOrganizator {

    private JPanel contentPane;
    JLabel timeLimit = new JLabel("Options: ");
    JLabel tenismenName = new JLabel("Player: ");
    JLabel gameId = new JLabel("Id: ");
    JLabel gameLocation = new JLabel("Location: ");
    JLabel gameHour = new JLabel("Hour:  ");
    JLabel refereeName = new JLabel("Referee: ");
    JTable table = new JTable();
    JComboBox<String> box = new JComboBox<String>();
    JComboBox<String> box1 = new JComboBox<String>();
    JComboBox<String> box3 = new JComboBox<String>();
    JTextField locationTf = new JTextField();
    JTextField gameHourTf = new JTextField();
    JTextField gameIdTf = new JTextField("-1");
    JButton addNewGame = new JButton("Add new game");
    JButton deleteAtIdBttn = new JButton("Delete");
    JButton updateComboBttn = new JButton("Update ComboBoxes");
    JButton updateTablesBttn = new JButton("Update");
    JButton viewAllGamesBttn = new JButton("View All Games");
    JScrollPane scroll_table = new JScrollPane(table);
    JButton infoBttn = new JButton("I");

    public ViewOrganizator() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(450, 180, 600, 700);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(null);
        setContentPane(contentPane);

        timeLimit.setBounds(180,10,200,40);
        contentPane.add(timeLimit);
        
        tenismenName.setBounds(40,130,90,30);
        contentPane.add(tenismenName);

        refereeName.setBounds(140,130,90,30);
        contentPane.add(refereeName);

        gameId.setBounds(240,130,90,30);
        contentPane.add(gameId);

        gameLocation.setBounds(340,130,90,30);
        contentPane.add(gameLocation);

        gameHour.setBounds(440,130,90,30);
        contentPane.add(gameHour);

        table.setBounds(40,130,90,30);

        scroll_table.setBounds(85, 250, 380, 250);
        scroll_table.setVisible(true);
        contentPane.add(scroll_table);

        box.setBounds(40, 160, 90, 30);
        contentPane.add(box);

        box1.setBounds(140, 160, 90, 30);
        contentPane.add(box1);

        box3.setBounds(205, 552, 110, 30);
        contentPane.add(box3);


        gameIdTf.setBounds(240,160,90,30);
        contentPane.add(gameIdTf);

        locationTf.setBounds(340,160,90,30);
        contentPane.add(locationTf);

        gameHourTf.setBounds(440,160,90,30);
        contentPane.add(gameHourTf);


        addNewGame.setFont(new Font("Tahoma", Font.PLAIN, 10));
        addNewGame.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                addNewGame();
            }
        });
        addNewGame.setBounds(40, 70, 150, 50);
        contentPane.add(addNewGame);

        updateComboBttn.setFont(new Font("Tahoma", Font.PLAIN, 10));

        updateComboBttn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                updateComboBoxes();
            }
        });
        updateComboBttn.setBounds(220, 70, 150, 50);
        contentPane.add(updateComboBttn);

        viewAllGamesBttn.setFont(new Font("Tahoma", Font.PLAIN, 10));
        viewAllGamesBttn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                viewAllGames();
            }
        });
        viewAllGamesBttn.setBounds(400, 70, 150, 50);
        contentPane.add(viewAllGamesBttn);

        infoBttn.setBorder(new RoundedBorder(7));
        infoBttn.setFont(new Font("Tahoma", Font.PLAIN, 8));

        infoBttn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setVisible(true);
                JOptionPane.showMessageDialog(infoBttn,"You have to set the id of the game you want to modify, "+"\n"+"to choose the field to be modified and to introduce the new value");
            }
        });
        infoBttn.setBounds(325, 550, 35, 35);
        contentPane.add(infoBttn);

        updateTablesBttn.setFont(new Font("Tahoma", Font.PLAIN, 10));
        updateTablesBttn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                updateTable();
            }
        });
        updateTablesBttn.setBounds(40, 545, 150, 50);
        contentPane.add(updateTablesBttn);

        deleteAtIdBttn.setFont(new Font("Tahoma", Font.PLAIN, 10));
        deleteAtIdBttn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                deleteAtId();
            }
        });
        deleteAtIdBttn.setBounds(40, 605, 150, 50);
        contentPane.add(deleteAtIdBttn);
    }

    @Override
    public void updateComboBoxes() {
        setVisible(true);

        for(Tenismen a: PresenterOrganizator.getListOfPlayers())
            box.addItem(a.getName());
        box.setVisible(true);

        for(Referee a: PresenterOrganizator.getListOfReferees())
            box1.addItem(a.getName());
        box1.setVisible(true);

        List<String> columns = new ArrayList<>();
        columns.add("playerName");
        columns.add("refereeName");
        columns.add("location");
        columns.add("hour");
        for(String a: columns)
            box3.addItem(a);


    }

    @Override
    public void addNewGame() {
        setVisible(true);
        Integer id = Integer.parseInt(gameIdTf.getText());
        String playerName = Objects.requireNonNull(box.getSelectedItem()).toString();
        String refereeName = Objects.requireNonNull(box1.getSelectedItem()).toString();
        String location = locationTf.getText();
        String hour = gameHourTf.getText();
        Game game = new Game(playerName,refereeName,location,hour);
        GameDAO.insert(game);
    }

    @Override
    public void viewAllGames() {
        setVisible(true);

        ConnectionClass cnn = GameDAO.printAllTable();
        table.setModel(DbUtils.resultSetToTableModel(cnn.getResult()));
        connection.ConnectionFactory.close(cnn.getStatement());
        connection.ConnectionFactory.close(cnn.getStatement());
        scroll_table.setVisible(true);
    }

    @Override
    public void updateTable() {
        setVisible(true);
        GameDAO g = new GameDAO();
        Integer id = Integer.parseInt(gameIdTf.getText());
        String value = null;
        if(box3.getSelectedItem() == "playerName")
            value = (String) box.getSelectedItem();
        if(box3.getSelectedItem() == "refereeName")
            value = (String) box1.getSelectedItem();
        if(box3.getSelectedItem() == "location")
            value = locationTf.getText();
        if(box3.getSelectedItem() == "hour")
            value = gameHourTf.getText();
        g.upadateAtId(id,value, (String) box3.getSelectedItem());
    }

    @Override
    public void deleteAtId() {
        setVisible(true);
        GameDAO g = new GameDAO();
        Integer id = Integer.parseInt(gameIdTf.getText());
        g.deleteById(id);
    }

}
