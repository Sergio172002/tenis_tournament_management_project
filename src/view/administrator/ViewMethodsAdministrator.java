package view.administrator;

public interface ViewMethodsAdministrator {
    public void viewAllGames();
    public void addNewOrganizator();
    public void addNewReferee();
    public void updateValues();
    public void updateComboBoxes();
}
