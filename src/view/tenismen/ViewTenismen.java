package view.tenismen;

import model.Tenismen;
import model.dao.ConnectionClass;
import model.dao.GameDAO;
import model.dao.TenismenDAO;
import net.proteanit.sql.DbUtils;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static java.lang.Integer.parseInt;

/**
 * Frame-ul corespunzator operatiilor efectuate
 * asupra tabelului Product al bazei de date
 */

public class ViewTenismen extends JFrame implements ViewMethodsTenismen {

    private JPanel contentPane;
    JLabel timeLimit = new JLabel("Name: ");
    JLabel tenismenName = new JLabel("Name: ");
    JLabel refereeName = new JLabel("WTA Position");
    JTable table = new JTable();
    JTextField nameTf = new JTextField();
    JTextField wtaPositionTf = new JTextField();
    JButton addNewGame = new JButton("Enroll: ");
    JButton viewAllGamesBttn = new JButton("View All Games");
    JScrollPane scroll_table = new JScrollPane(table);

    public ViewTenismen() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(450, 180, 600, 700);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(null);
        setContentPane(contentPane);

        timeLimit.setBounds(180,10,200,40);
        contentPane.add(timeLimit);

        tenismenName.setBounds(40,130,90,30);
        contentPane.add(tenismenName);

        refereeName.setBounds(140,130,90,30);
        contentPane.add(refereeName);

        table.setBounds(40,130,90,30);

        scroll_table.setBounds(85, 250, 380, 250);
        scroll_table.setVisible(true);
        contentPane.add(scroll_table);

        nameTf.setBounds(40,160,90,30);
        contentPane.add(nameTf);

        wtaPositionTf.setBounds(140,160,90,30);
        contentPane.add(wtaPositionTf);


        addNewGame.setFont(new Font("Tahoma", Font.PLAIN, 10));
        addNewGame.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                insertPlayer();
            }
        });
        addNewGame.setBounds(40, 70, 150, 50);
        contentPane.add(addNewGame);

        viewAllGamesBttn.setFont(new Font("Tahoma", Font.PLAIN, 10));
        viewAllGamesBttn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                viewAllGames();
            }
        });
        viewAllGamesBttn.setBounds(240, 70, 150, 50);
        contentPane.add(viewAllGamesBttn);

    }

    @Override
    public void insertPlayer() {
        String name = nameTf.getText();
        Integer wtaPosition = Integer.valueOf(wtaPositionTf.getText());
        Tenismen tenismen = new Tenismen(name, wtaPosition);
        TenismenDAO t = new TenismenDAO();
        TenismenDAO.insert(tenismen);
    }

    @Override
    public void viewAllGames() {
        ConnectionClass cnn = GameDAO.printAllTable();
        table.setModel(DbUtils.resultSetToTableModel(cnn.getResult()));
        connection.ConnectionFactory.close(cnn.getStatement());
        connection.ConnectionFactory.close(cnn.getStatement());
        scroll_table.setVisible(true);
    }
}
