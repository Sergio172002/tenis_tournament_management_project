package view.tenismen;

public interface ViewMethodsTenismen {
    public void insertPlayer();
    public void viewAllGames();
}
