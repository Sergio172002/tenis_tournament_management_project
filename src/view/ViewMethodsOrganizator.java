package view;

public interface ViewMethodsOrganizator {
    public void updateComboBoxes();
    public void addNewGame();
    public void viewAllGames();
    public void updateTable();
    public void deleteAtId();
}
