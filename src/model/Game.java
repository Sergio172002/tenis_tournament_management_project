package model;

public class Game {

    private int id;
    private String playerName;
    private String refereeName;
    private String location;
    private String hour;

    public Game(){

    }
    public Game(int id, String playerName, String refereeName, String location, String hour){
        this.id = id;
        this.playerName = playerName;
        this.refereeName = refereeName;
        this.location = location;
        this.hour = hour;
    }
    public Game(String playerName, String refereeName, String location, String hour){
        this.playerName = playerName;
        this.refereeName = refereeName;
        this.location = location;
        this.hour = hour;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getRefereeName() {
        return refereeName;
    }

    public void setRefereeName(String refereeName) {
        this.refereeName = refereeName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    @Override
    public String toString() {
        return "Game{" +
                "id=" + id +
                ", playerName='" + playerName + '\'' +
                ", refereeName='" + refereeName + '\'' +
                ", location='" + location + '\'' +
                ", hour='" + hour + '\'' +
                '}';
    }
}
