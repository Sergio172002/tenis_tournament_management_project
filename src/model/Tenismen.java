package model;

public class Tenismen {

    private int id;
    private String name;
    private int wtaPosition;

    public Tenismen(){}

    public Tenismen(int id, String name, int wtaPosition){
        this.id = id;
        this.name = name;
        this.wtaPosition=wtaPosition;
    }

    public Tenismen(String name, int wtaPosition){
        this.name = name;
        this.wtaPosition = wtaPosition;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWtaPosition() {
        return wtaPosition;
    }

    public void setWtaPosition(int wtaPosition) {
        this.wtaPosition = wtaPosition;
    }

    @Override
    public String toString() {
        return "Tenismen{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", wtaPosition=" + wtaPosition +
                '}';
    }
}

