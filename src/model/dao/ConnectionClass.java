package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
/** Aceasta clasa incpsuleaza caracteristicile conexinuii
 */


public class ConnectionClass {

    ResultSet result;
    PreparedStatement statement;
    Connection connection;

    /**
     * <p>Constructorul clasei ConnectionClass</p>
     * @param result  resutl
     * @param statement statment
     * @param connection connection
     */

    public ConnectionClass(ResultSet result, PreparedStatement statement, Connection connection)
    {
        this.result = result;
        this.statement = statement;
        this.connection = connection;
    }

    /**
     * <p>Getter-ul clasei</p>
     * @return result
     */

    public ResultSet getResult() {
        return result;
    }
    /**
     * <p>Setter-ul clasei</p>
     * @param result
     */
    public void setResult(ResultSet result) {
        this.result = result;
    }

    /**
     * <p>Getter pentru statement</p>
     * @return statement
     */
    public PreparedStatement getStatement() {
        return statement;
    }


    /**
     * <p>Settrul pentru statement</p>
     * @param statement  className
     */
    public void setStatement(PreparedStatement statement) {
        this.statement = statement;
    }

    /**
     * <p>Gettrul pentru connection</p>
     * @return connection
     */
    public Connection getConnection() {
        return connection;
    }

    /**
     * <p>Settrul pentru connection</p>
     * @param connection connection
     */

    public void setConnection(Connection connection) {
        this.connection = connection;
    }
}
