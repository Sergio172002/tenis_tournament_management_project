package model.dao;

import connection.ConnectionFactory;
import model.Game;
import model.Referee;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 * Aceasta clasa se ocupa cu crearea de metode specifice doar pentru clasa Client si
 * adaptarea acelora din AbstractDAO
 */

public class RefereeDAO extends AbstractDAO<Referee>{

    private static final Logger LOGGER = Logger.getLogger(RefereeDAO.class.getName());
    private static final String insertStatementString = "INSERT INTO referee(name,password)" + " VALUES (?,?)";
    private final static String printAllTableString = "SELECT * FROM referee";
    private final static String printSomeValues = "SELECT * FROM game WHERE refereeName = ?";

    /**
     * Constructorul clasei ClientDAO
     */

    public RefereeDAO()
    {
        super();
    }

    /**
     * <p>Metoda string-ului folosit de instructiunea SQL SELECT</p>
     * @return ConnectionClass
     */

    public static ConnectionClass printAllTable() {

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(printAllTableString);
            rs = findStatement.executeQuery();

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING,"RefereeDAO:printAllTable " + e.getMessage());
        }
        ConnectionClass cnn = new ConnectionClass(rs, findStatement, dbConnection);
        return cnn;

    }

    public static ConnectionClass printPartTable(String name) {

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(printSomeValues);
            findStatement.setString(1, name);
            rs = findStatement.executeQuery();

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING,"TenismanDAO:printAllTable " + e.getMessage());
        }
        ConnectionClass cnn = new ConnectionClass(rs, findStatement, dbConnection);
        return cnn;

    }
    public static int insert(Referee referee) {
        Connection dbConnection = ConnectionFactory.getConnection();

        PreparedStatement insertStatement = null;
        int insertedId = -1;
        try {
            insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
            insertStatement.setString(1, referee.getName());
            insertStatement.setString(2, referee.getPassword());
            insertStatement.executeUpdate();

            ResultSet rs = insertStatement.getGeneratedKeys();
            if (rs.next()) {
                insertedId = rs.getInt(1);
            }

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "StudentDAO: insert " + e.getMessage());
        } finally {
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }
        return insertedId;
    }
}
