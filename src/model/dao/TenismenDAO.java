package model.dao;

import connection.ConnectionFactory;
import model.Game;
import model.Tenismen;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 * Aceasta clasa se ocupa cu crearea de metode specifice doar pentru clasa Client si
 * adaptarea acelora din AbstractDAO
 */

public class TenismenDAO extends AbstractDAO<Tenismen>{

    private static final Logger LOGGER = Logger.getLogger(TenismenDAO.class.getName());
    private static final String insertStatementString = "INSERT INTO tenismen(name,wtaPosition)" + " VALUES (?,?)";
    private final static String printAllTableString = "SELECT * FROM tenismen";

    /**
     * Constructorul clasei ClientDAO
     */

    public TenismenDAO()
    {
        super();
    }

    /**
     * <p>Metoda string-ului folosit de instructiunea SQL SELECT</p>
     * @return ConnectionClass
     */

    public static ConnectionClass printAllTable() {

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(printAllTableString);
            rs = findStatement.executeQuery();

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING,"TenismanDAO:printAllTable " + e.getMessage());
        }
        ConnectionClass cnn = new ConnectionClass(rs, findStatement, dbConnection);
        return cnn;

    }

    public static int insert(Tenismen tenismen) {
        Connection dbConnection = ConnectionFactory.getConnection();

        PreparedStatement insertStatement = null;
        int insertedId = -1;
        try {
            insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
            insertStatement.setString(1, tenismen.getName());
            insertStatement.setInt(2, tenismen.getWtaPosition());
            insertStatement.executeUpdate();

            ResultSet rs = insertStatement.getGeneratedKeys();
            if (rs.next()) {
                insertedId = rs.getInt(1);
            }

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "StudentDAO: insert " + e.getMessage());
        } finally {
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }
        return insertedId;
    }
}
