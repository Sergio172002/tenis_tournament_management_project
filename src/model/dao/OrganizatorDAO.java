package model.dao;

import connection.ConnectionFactory;
import model.Organizator;
import model.Referee;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 * Aceasta clasa se ocupa cu crearea de metode specifice doar pentru clasa Client si
 * adaptarea acelora din AbstractDAO
 */

public class OrganizatorDAO extends AbstractDAO<Organizator>{

    private static final Logger LOGGER = Logger.getLogger(OrganizatorDAO.class.getName());
    private static final String insertStatementString = "INSERT INTO organizator(name, password)" + " VALUES (?,?)";
    private final static String printAllTableString = "SELECT * FROM organizator";

    /**
     * Constructorul clasei ClientDAO
     */

    public OrganizatorDAO()
    {
        super();
    }

    /**
     * <p>Metoda string-ului folosit de instructiunea SQL SELECT</p>
     * @return ConnectionClass
     */

    public static ConnectionClass printAllTable() {

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(printAllTableString);
            rs = findStatement.executeQuery();

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING,"StudentDAO:printAllTable " + e.getMessage());
        }
        ConnectionClass cnn = new ConnectionClass(rs, findStatement, dbConnection);
        return cnn;

    }
    public static int insert(Organizator organizator) {
        Connection dbConnection = ConnectionFactory.getConnection();

        PreparedStatement insertStatement = null;
        int insertedId = -1;
        try {
            insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
            insertStatement.setString(1, organizator.getName());
            insertStatement.setString(2, organizator.getPassword());
            insertStatement.executeUpdate();

            ResultSet rs = insertStatement.getGeneratedKeys();
            if (rs.next()) {
                insertedId = rs.getInt(1);
            }

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "StudentDAO: insert " + e.getMessage());
        } finally {
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }
        return insertedId;
    }}

