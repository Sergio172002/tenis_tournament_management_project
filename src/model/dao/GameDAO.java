package model.dao;

import connection.ConnectionFactory;
import model.Game;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 * Aceasta clasa se ocupa cu crearea de metode specifice doar pentru clasa Client si
 * adaptarea acelora din AbstractDAO
 */

public class GameDAO extends AbstractDAO<Game>{

    private static final Logger LOGGER = Logger.getLogger(GameDAO.class.getName());
    private static final String insertStatementString = "INSERT INTO game(playerName,refereeName,location, hour)" + " VALUES (?,?,?,?)";
    private final static String printAllTableString = "SELECT * FROM game";

    /**
     * Constructorul clasei ClientDAO
     */

    public GameDAO()
    {
        super();
    }

    /**
     * <p>Metoda string-ului folosit de instructiunea SQL SELECT</p>
     * @return ConnectionClass
     */

    public static ConnectionClass printAllTable() {

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(printAllTableString);
            rs = findStatement.executeQuery();

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING,"StudentDAO:printAllTable " + e.getMessage());
        }
        ConnectionClass cnn = new ConnectionClass(rs, findStatement, dbConnection);
        return cnn;

    }


    public static int insert(Game game) {
        Connection dbConnection = ConnectionFactory.getConnection();

        PreparedStatement insertStatement = null;
        int insertedId = -1;
        try {
            insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
            insertStatement.setString(1, game.getPlayerName());
            insertStatement.setString(2, game.getRefereeName());
            insertStatement.setString(3, game.getLocation());
            insertStatement.setString(4, game.getHour());
            insertStatement.executeUpdate();

            ResultSet rs = insertStatement.getGeneratedKeys();
            if (rs.next()) {
                insertedId = rs.getInt(1);
            }

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "StudentDAO: insert " + e.getMessage());
        } finally {
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }
        return insertedId;
    }

}

