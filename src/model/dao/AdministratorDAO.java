package model.dao;

import connection.ConnectionFactory;
import model.Administrator;
import model.Organizator;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 * Aceasta clasa se ocupa cu crearea de metode specifice doar pentru clasa Client si
 * adaptarea acelora din AbstractDAO
 */

public class AdministratorDAO extends AbstractDAO<Administrator>{

    private static final Logger LOGGER = Logger.getLogger(OrganizatorDAO.class.getName());
    private static final String insertStatementString = "INSERT INTO client(name,address,email)" + " VALUES (?,?,?)";
    private final static String printAllTableString = "SELECT * FROM administrator";

    /**
     * Constructorul clasei ClientDAO
     */

    public AdministratorDAO()
    {
        super();
    }

    /**
     * <p>Metoda string-ului folosit de instructiunea SQL SELECT</p>
     * @return ConnectionClass
     */

    public static ConnectionClass printAllTable() {

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(printAllTableString);
            rs = findStatement.executeQuery();

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING,"StudentDAO:printAllTable " + e.getMessage());
        }
        ConnectionClass cnn = new ConnectionClass(rs, findStatement, dbConnection);
        return cnn;

    }
/*
    /**
     * <p>Metoda string-ului folosit de instructiunea SQL SELECT</p>
     * @param client  ClientObj
     * @return SQLquery
     */
/*
    public static int insert(Client client) {
        Connection dbConnection = ConnectionFactory.getConnection();

        PreparedStatement insertStatement = null;
        int insertedId = -1;
        try {
            insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
            insertStatement.setString(1, client.getName());
            insertStatement.setString(2, client.getAddress());
            insertStatement.setString(3, client.getEmail());
            insertStatement.executeUpdate();

            ResultSet rs = insertStatement.getGeneratedKeys();
            if (rs.next()) {
                insertedId = rs.getInt(1);
            }

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "StudentDAO: insert " + e.getMessage());
        } finally {
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }
        return insertedId;
    }
*/
}

