package model.dao;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;

/**
 * Aceasta clasa se ocupa cu crearea de metode abstracte care vor
 * fi folosite prin reflectie de catre obiecte diferite
 */
public class AbstractDAO<T> {
    protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());

    private final Class<T> type;

    @SuppressWarnings("unchecked")
    public AbstractDAO() {
        this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

    }
    /**
     * <p>Metoda string-ului folosit de instructiunea SQL SELECT</p>
     * @param field  className
     * @return SQLquery
     */

    private String createSelectQuery(String field) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append(" * ");
        sb.append(" FROM ");
        sb.append(type.getSimpleName());
        sb.append(" WHERE " + field + " =?");
        return sb.toString();
    }

    private String createSelectQueryNameAndPass(String field1, String field2) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append(" * ");
        sb.append(" FROM ");
        sb.append(type.getSimpleName());
        sb.append(" WHERE " + field1 + " =? " +" AND " + field2+" =? ");
        return sb.toString();
    }

    /**
     * <p>Metoda string-ului folosit de instructiunea SQL SELECT ALL</p>
     * @return SQLquery
     */

    private String createSelectAllQuery()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append(" * ");
        sb.append(" FROM ");
        sb.append(type.getSimpleName());
        return sb.toString();
    }
    /**
     * <p>Metoda string-ului folosit de instructiunea SQL DELETE</p>
     * @param field className
     * @return SQLquery
     */

    private String createDeleteQuery(String field)
    {
        StringBuilder sb = new StringBuilder();
        sb.append("DELETE ");
        sb.append(" FROM ");
        sb.append(type.getSimpleName());
        sb.append(" WHERE " + field + " = ?");
        return sb.toString();
    }

    /**
     * <p>Metoda string-ului folosit de instructiunea SQL UPDATE</p>
     * @param field1 className
     * @param field2 columnName
     * @return SQLquery
     */

    private String createUpdateQuery(String field1, String field2)
    {
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE ");
        sb.append(type.getSimpleName());
        sb.append(" SET ");
        sb.append(field2+" = "+"?");
        sb.append(" WHERE " + field1 + " = ?");
        return sb.toString();
    }

    /**
     * <p>Metoda string-ului folosit de instructiunea SQL SELECT UPDATE AT ID</p>
     * @param id remId
     * @param amtToRemove quantity
     */

    public void upadateAtId(int id, String amtToRemove, String columnName) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createUpdateQuery("id", columnName);
        System.out.println(query);
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setString(1, amtToRemove);
            statement.setInt(2, id);
            System.out.println(statement+"");
            statement.executeUpdate();

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "StudentDAO:deleteById " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
    }
public List<T> findByNameAndPass(String name, String password) {
    Connection connection = null;
    PreparedStatement statement = null;
    ResultSet resultSet = null;
    String query = createSelectQueryNameAndPass("name", "password");
    try {
        connection = ConnectionFactory.getConnection();
        statement = connection.prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, password);
        resultSet = statement.executeQuery();

        return createObjects(resultSet);
    } catch (SQLException e) {
        LOGGER.log(Level.WARNING, type.getName() + "DAO:findByNameAndPass " + e.getMessage());
    } finally {
        ConnectionFactory.close(resultSet);
        ConnectionFactory.close(statement);
        ConnectionFactory.close(connection);
    }
    return null;
}

    /**
     * <p>Metoda care gasesete un camp din baza de date dupa id-ul sau</p>
     * @param id idToBeFound
     * @return Object
     */
    public T findById(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createSelectQuery("id");
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();

            return createObjects(resultSet).get(0);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }
    /**
     * <p>Metoda care sterge un camp din baza de date dupa id-ul sau</p>
     * @param id idToBeDeleted
     */

    public void deleteById(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createDeleteQuery("id");
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            statement.executeUpdate();

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "StudentDAO:deleteById " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
    }

    /**
     * <p>Metoda care returneaza o lista cu toate obiectele din baza de date</p>
     * @return ListOfObjects
     */

    public List<T> findAll() {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query =  createSelectAllQuery();
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            resultSet = statement.executeQuery();

            return createObjects(resultSet);

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING,"StudentDAO:printAllTable " + e.getMessage());
        }finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);

        }
        return null;
    }

    /**
     * <p>Metoda care returneaza o lista de obiecte dupa resultSet ul pe care il primeste</p>
     * @param rs ResultSet
     * @return Object
     */
    private List<T> createObjects(ResultSet rs) {
        List<T> list= new ArrayList<T>();
        try{
            while(rs.next()){
                T instance= type.getDeclaredConstructor().newInstance();
                for(Field field: type.getDeclaredFields()){
                    Object value=rs.getObject(field.getName());
                    PropertyDescriptor propertyDescriptor=new PropertyDescriptor(field.getName(),type);
                    Method method=propertyDescriptor.getWriteMethod();
                    method.invoke(instance, value);
                }
                list.add(instance);
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IntrospectionException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return list;
    }

}
