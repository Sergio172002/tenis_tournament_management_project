package connection;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * ConnectionFactory este clasa care creeaza conexiunea cu baza de date din mysql
 */

public class ConnectionFactory {

    private static final Logger LOGGER = Logger.getLogger(ConnectionFactory.class.getName());
    private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
    /**
     * URL pentru conectarea cu baza de date
     */
    private static final String DBURL = "jdbc:mysql://localhost:3306/tenistournament";
    /**
     * User-ul
     */
    private static final String USER = "root";
    /**
     * Password
     */
    private static final String PASS = "poroseni";

    private static ConnectionFactory singleInstance = new ConnectionFactory();

    private ConnectionFactory()
    {
        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
        }catch(ClassNotFoundException e){
            e.printStackTrace();
        }
    }
    /**
     * <p>Metoda pentru crearea conexiunii cu baza de date</p>
     * @return conexiunea
     */

    private Connection createConnection() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/tenistournament", "root", "poroseni");
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "An error occured while trying to connect to the database");
            e.printStackTrace();
        }
        return connection;
    }
    /**
     * <p>Metoda pentru obtinerea conexiunii</p>
     * @return conexiunea
     */

    public static Connection getConnection()
    {
        return singleInstance.createConnection();
    }

    /**
     * <p>Metoda inchide conexiunea primita ca parametru</p>
     * @param connection conexiunea
     */

    public static void close(Connection connection)
    {
        if(connection != null)
        {
            try {
                connection.close();
            }catch(SQLException e){
                LOGGER.log(Level.WARNING,"An error occured while trying to close the connection");
            }

        }
    }
    /**
     * <p>Metoda pentru inchiderea unui statement primit ca parametru</p>
     * @param statement
     */

    public static void close(Statement statement)
    {
        if(statement != null)
        {
            try {
                statement.close();
            }catch(SQLException e){
                LOGGER.log(Level.WARNING,"An error occured while trying to close the connection");
            }
        }
    }
    /**
     * <p>Metoda pentru inchiderea unui ResultSet primit ca parametru</p>
     * @param resultSet
     */

    public static void close(ResultSet resultSet)
    {
        if(resultSet != null)
        {
            try {
                resultSet.close();
            }catch(SQLException e){
                LOGGER.log(Level.WARNING,"An error occured while trying to close the connection");
            }
        }
    }


}
