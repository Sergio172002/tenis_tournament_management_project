package model.dao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AbstractDAOTest {

    @Test
    void upadateAtId() {
        GameDAO g = new GameDAO();
        g.upadateAtId(1,"Paris2", "location");
        Assertions.assertEquals("Game{id=1, playerName='Nadal', refereeName='Kovacs', location='Paris2', hour='12:00'}", g.findById(1).toString());
    }

    @Test
    void findByNameAndPass() {
        RefereeDAO g = new RefereeDAO();
        Assertions.assertEquals("[Referee{id=1, name='arbitru1', password='as'}]", g.findByNameAndPass("arbitru1", "as").toString());
    }

    @Test
    void findById() {
        RefereeDAO g = new RefereeDAO();
        Assertions.assertEquals("Referee{id=1, name='arbitru1', password='as'}", g.findById(1).toString());
    }


    @Test
    void findAll() {
        AdministratorDAO g = new AdministratorDAO();
        System.out.println(g.findAll().toString());
        Assertions.assertEquals("[Administrator{id=1, name='a', password='a'}, Administrator{id=2, name='b', password='b'}]", g.findAll().toString());
    }
}